*========================================================================================
* Version history:
*
* 2007-02Feb-19: Implemented EncodeMemo and DecodeMemo to deal with extended properties
*                in VFP 9
*
* 2007-06Jun-21: Fixed the problem with report files and label files
*                Fixed an issue with extended properties in forms
*                Output in classes is now sorted to retain a constant order when
*                comparing files
*                Fixed an issue with forms that only have a single method
*
* 2007-08Aug-08: Fixed an issue with corrupt VCX files
*
* 2008-09Sep-30: Fixed the problem with ZOrder changes when generating code. Added
*                the SORT_METHODS switch below to reverse to the old behavior.
*                Fixed the problem with CDATA sections in converted source code
*                Fixed the issue of binary data in methods
*
* 2009-07Jul-14: Refactoring to minimize code duplication
*                Added support for MNX files
*                Removed the confirmation dialog in GenCode
*
* 2010-01Jan-18: When generating text files only modified files are analysed.
*
* 2010-03Mar-08: Removed fields from PJX output that depend on the location or the
*                developer's machine
*                Removed SORT_METHODS switch. 
*                ZOrder is now properly handled without that classes move around in
*                the file.
*                GenXML accepts a second parameter. When it's .T., all files are
*                updated regardless of any time stamp. Use this option when you 
*                introduce a new version of TwoFox.
*
*                NOTE: This version produces slightly different XML files than
*                      previous versions. Run GenXML once to produce new XML files
*                      and check them into your version control system as a reference.
*                      Then make changes and generate files as usual.
* 
* 2010-03Mar-16: OLE2 field in classes and forms is stripped to the file name only,
*                if OCX not located in project directory
*                Removed conversion of LF to CRLF. Was needed in VFP 7.
*                Encoded USER field.
*                Fix for wwIpStuff.vcx with binary characters in icon names
*
* 2011-06Jun-18: Added back conversion from LF to CRLF. Is still needed.
*
* 2013-10Oct-28: Added encoding/decoding protected field in classlibs
*
* Acknowledgements:
*
* Many thanks to (in no particular order)
*
*  - Ulli Gellesch
*  - John Beard
*  - Larry Bradley
*  - Jorge Mota
*  - Matthew Osborn
*  - Peter Steinke 
*  - Toni Feltman
*  - Steve Sawyer
*  - Bogdan Zamfir
*  - Alan Stevens
*  - Thorsten Hoffmann
*  - Shane Charles 
* 
* for finding bugs, offering solutions, and making suggestions.
*========================================================================================


*========================================================================================
* Base class for creating binary files from the XML represenation
*========================================================================================
Define Class CMerge as Session
	
	DataSession = 2

	lForce = .F.
	cHomeDir = ""
	
	Procedure Convert(tcFile)
	EndProc
	
	Procedure Init
		Set Deleted on
	EndProc 
	
	Procedure Msg(tcText)
		Wait WINDOW nowait m.tcText
	EndProc 
	
*========================================================================================
* Creates a backup of the existing file
*========================================================================================
Procedure BackupExisting
Lparameters tcFile
	
	*--------------------------------------------------------------------------------------
	* We place all backup files into a folder beneath the current directory to avoid
	* clutter in the project directory and to make it easier to remove old files
	*--------------------------------------------------------------------------------------
	Local lcDir
	lcDir = Addbs(JustPath(m.tcFile)) + ".twofox"
	If not Directory(m.lcDir,1)
		MkDir (m.lcDir)
	EndIf 
	
	Local lcNew
	If File(m.tcFile)
		lcNew = Addbs(m.lcDir)+TtoC(Datetime(),1)+"."+JustFname(m.tcFile)
		Rename (m.tcFile) to (m.lcNew)
	EndIf
	
EndProc 

*========================================================================================
* Replaces binary characters 
*========================================================================================
Procedure DecodeMemo( tcMemo )
	
	Local lcMemo, lnCode
	lcMemo = m.tcMemo
	
	* Some memo fields such as the PROPERTIES field only contain CHR(10) after
	* being converted by XmlToCursor. We ensure that we only use CRLF, but don't 
	* accidentally convert existing CRLFs.
	lcMemo = STRTRAN (m.lcMemo, CHR(13)+CHR(10), CHR(10))
	lcMemo = STRTRAN (m.lcMemo, CHR(10), CHR(13)+CHR(10))
	
	For m.lnCode=0 to 31
		lcMemo = Strtran(m.lcMemo,"{"+Transform(m.lnCode)+"}",Chr(m.lnCode))
	EndFor
	lcMemo = Strtran(m.lcMemo,"{33}","!")
	lcMemo = Strtran(m.lcMemo,"{93}","]")

	If "{123}" $ m.lcMemo
		lcMemo = Strtran(m.lcMemo,"{123}","{")
	EndIf 

Return m.lcMemo

EndDefine


*========================================================================================
* Handles VCX classes
*========================================================================================
Define Class CMergeVCX as CMerge

	cFile = ""

*========================================================================================
* Creates a class library from a set of XML files.
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile+".xml")
		This.BackupExisting( This.cFile )
		This.BackupExisting( ForceExt(This.cFile,"vct") )
		Create Classlib (This.cFile)
		USE (This.cFile) Alias VCX Shared In Select("VCX")
		This.LoadHeader()
		Select ClassList
		Scan
			This.LoadClass( Alltrim(ClassList.ObjName) )
		EndScan 
		USE in Select("VCX")
		USE in Select("ClassList")
		USE in Select("Class")
		Compile CLASSLIB (This.cFile)
	EndIf 
EndProc


*========================================================================================
* Loads the XML file that lists all classes
*========================================================================================
Procedure LoadHeader
	This.Msg( JustFname(This.cFile) )
	XMLToCursor( This.cFile+".xml", "ClassList", 512 )
EndProc


*========================================================================================
* Inserts a class from an XML file
*
* Use a SCAN loop with a temporary index here, because one version of TwoFox might have
* created an XML file where records are in the wrong order.
*========================================================================================
Procedure LoadClass
LParameter tcClass
	Local loRecord
	This.Msg( JustFname(This.cFile)+"."+m.tcClass )
	USE in Select("Class")
	XMLToCursor( FileToStr(This.cFile+"."+m.tcClass+".xml"), "Class" )
	Select Class
	Replace all ;
		 Properties with This.DecodeMemo(Properties) ;
		,Methods with This.DecodeMemo(Methods) ;
		,User with This.DecodeMemo(User) ;
		,Reserved3 with This.DecodeMemo(Reserved3) ;
		,Reserved4 with This.DecodeMemo(Reserved4) ;
		,Reserved5 with This.DecodeMemo(Reserved5) ;
		,Protected WITH This.DecodeMemo(Protected) 
	
	If Type("nRecNo") == "N"
		Index on nRecNo Tag _Fixed 
	EndIf
	Scan 
		Scatter name loRecord Memo 
		Select VCX
		Append Blank
		Gather name loRecord Memo
	EndScan
EndProc

EndDefine 


*========================================================================================
* Handles SCX forms
*========================================================================================
Define Class CMergeSCX as CMerge

	cFile = ""

*========================================================================================
* Creates a form from an XML files.
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile+".xml")
		This.BackupExisting( This.cFile )
		This.BackupExisting( ForceExt(This.cFile,"sct") )
		This.LoadForm()
		Compile Form (This.cFile)
	EndIf 
EndProc


*========================================================================================
* Inserts a class from an XML file
*========================================================================================
Procedure LoadForm
LParameter tcClass
	This.Msg( JustFname(This.cFile) )
	XMLToCursor( FileToStr(This.cFile+".xml"), "Form" )
	Select Form
	Replace all ;
		 Properties with This.DecodeMemo(Properties) ;
		,Methods with This.DecodeMemo(Methods) ;
		,User with This.DecodeMemo(User) ;
		,Reserved3 with This.DecodeMemo(Reserved3) ;
		,Reserved4 with This.DecodeMemo(Reserved4) ;
		,Reserved5 with This.DecodeMemo(Reserved5) 

	If Type("nRecNo") == "N"
		Select * from Form order by nRecNo into cursor Form2 Nofilter readwrite
		Alter table Form2 drop column nRecNo
		Select Form2
	EndIf 
	Copy To (This.cFile)

	Use in Select("For2m")
	Use in Select("Form")
EndProc

EndDefine 



*========================================================================================
* Handles Projects
*========================================================================================
Define Class CMergePJX as CMerge

	cFile = ""

*========================================================================================
* Creates a project from an XML files.
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile)
		This.BackupExisting( ForceExt(This.cFile,"pjx") )
		This.BackupExisting( ForceExt(This.cFile,"pjt") )
		This.LoadProject()
	EndIf 
EndProc


*========================================================================================
* Recreates the project file from the XML string 
*========================================================================================
Procedure LoadProject
LParameter tcClass
	Set Safety off
	This.Msg( JustFname(This.cFile) )
	XMLToCursor( FileToStr(This.cFile), "curProject" )
	StrToFile("*","x.prg")
	Build Project (ForceExt(This.cFile,"pjx")) from x.prg
	Erase x.prg
	Use (ForceExt(This.cFile,"pjx")) Alias Project Exclusive In Select("Project")
	Select Project
	Zap
	Append From Dbf("curProject")
	Use in Select("curProject")
	
	*--------------------------------------------------------------------------------------
	* We removed some field values that depend on the project's location. Insert correct
	* values based on current location.
	*--------------------------------------------------------------------------------------
	Replace all for Type == "H" ;
		 Reserved1 with Dbf("Project") ;
		,Object with JustPath(This.cFile) ;
		,Name with Dbf("Project") ;
		,HomeDir with JustPath(This.cFile)
	
	Use in Select("Project")
ENDPROC



EndDefine 


*========================================================================================
* Handles FRX forms
*========================================================================================
Define Class CMergeFRX as CMerge

	cFile = ""

*========================================================================================
* Creates a form from an XML files.
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile+".xml")
		This.BackupExisting( This.cFile )
		This.BackupExisting( ForceExt(This.cFile,"frt") )
		This.LoadReport()
		Compile Report (This.cFile)
	EndIf 
EndProc


*========================================================================================
* Inserts a class from an XML file
*========================================================================================
Procedure LoadReport
LParameter tcClass
	This.Msg( JustFname(This.cFile) )
	XMLToCursor( FileToStr(This.cFile+".xml"), "Report" )
	Select 0
	Create Cursor curDummy (cField C(10))
	Create Report (This.cFile) from Dbf("curDummy")
	Use in Select("curDummy")
	Use (This.cFile) Again Alias __reportnew Exclusive
	Zap
	Append From Dbf("Report")
	Use in ("Report")
	Use in ("__reportnew")
EndProc


EndDefine 


*========================================================================================
* Handles MNX menus
*========================================================================================
Define Class CMergeMNX as CMerge

	cFile = ""

*========================================================================================
* Creates a menu from an XML files.
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile+".xml")
		This.BackupExisting( This.cFile )
		This.BackupExisting( ForceExt(This.cFile,"mnt") )
		This.LoadMenu()
	EndIf 
EndProc


*========================================================================================
* Performs the actual conversion from an XML file into an MNX file
*========================================================================================
Procedure LoadMenu
	This.Msg( JustFname(This.cFile) )
	XMLToCursor( FileToStr(This.cFile+".xml"), "Menu" )
	Select Menu

	Replace all Mark with This.DecodeMemo(Mark)
	Replace all Procedure with This.DecodeMemo(Procedure)
	Replace all Prompt with This.DecodeMemo(Prompt)
	Replace all ItemNum with Padl(Alltrim(ItemNum),3)

	Alter Table Menu alter column Mark C(1)

	Copy To (This.cFile)
	Use in ("Menu")
EndProc


EndDefine 


*========================================================================================
* Generic Splitting class
*========================================================================================
Define Class CSplit as Session
	
	DataSession = 2
	
	lForce = .F.
	cHomeDir = ""
	
	Procedure Convert(tcFile)
	EndProc
	
	Procedure Init
		Set Deleted on
	EndProc 
	
	Procedure Msg(tcText)
		Wait WINDOW nowait m.tcText
	EndProc 
	
	*========================================================================================
	* Produces an XML file from an existing cursor.
	*========================================================================================
	Procedure CursorToXML
	Lparameters tcAlias, tcFile
		Local lcXML, lcOldContent
		CursorToXML( m.tcAlias, "lcXML", 1, 0+8, 0, "1", "", "" )
		lcXML = Strtran( m.lcXML, ;
			[<xsd:choice maxOccurs="unbounded">]+Chr(13)+Chr(10)+Replicate(Chr(9),5)+[<xsd:element name="class">], ;
			[<xsd:choice maxOccurs="unbounded">]+Chr(13)+Chr(10)+Replicate(Chr(9),5)+[<xsd:element name="class" minOccurs="0" maxOccurs="unbounded">] ;
		)
		If not [<xsd:anyAttribute namespace="http://www.w3.org/XML/1998/namespace" processContents="lax"/>] $ m.lcXML
			lcXML = Strtran( m.lcXML, ;
				[</xsd:choice>]+Chr(13)+Chr(10), ;
				[</xsd:choice>]+Chr(13)+Chr(10)+Replicate(Chr(9),4)+[<xsd:anyAttribute namespace="http://www.w3.org/XML/1998/namespace" processContents="lax"/>]+Chr(13)+Chr(10) ;
			)
		EndIf 
		If File(m.tcFile)
			lcOldContent = FileToStr(m.tcFile)
		Else
			m.lcOldContent = ""
		EndIf
		If not m.lcOldContent == m.lcXML
			StrToFile( m.lcXML, m.tcFile )
		EndIf 
	EndProc 

*========================================================================================
* Replaces binary characters 
*========================================================================================
Procedure EncodeMemo( tcMemo )
	
	If Empty(m.tcMemo)
		Return m.tcMemo
	EndIf
	
	Local lcMemo, lnCode
	lcMemo = m.tcMemo
	
	If "{" $ m.lcMemo
		lcMemo = Strtran(m.lcMemo,"{","{123}")
	EndIf 
	lcMemo = Strtran(m.lcMemo,Chr(13)+Chr(10),"{1310}")
	For m.lnCode=0 to 31
		IF m.lnCode != 9
			lcMemo = Strtran(m.lcMemo,Chr(m.lnCode),"{"+Transform(m.lnCode)+"}")
		ENDIF
	EndFor
	lcMemo = Strtran(m.lcMemo,"{1310}",Chr(13)+Chr(10))

	lcMemo = STRTRAN( m.lcMemo, "<![CDATA[>", "<{33}[CDATA[>" )
	lcMemo = STRTRAN( m.lcMemo, "]]>", "{93}{93}>" )

Return m.lcMemo

	
*========================================================================================
* Compares the time stamp of the original file and the generated text file. Returns .T.
* if the generated text file is out-dated.
*
* Note: The current implementation reports a change every time the project is completely
*        recompiled. We do not check the time stamps inside the file.
*========================================================================================
Procedure HasChanged (tcFile, tcText)

	*--------------------------------------------------------------------------------------
	* lForce controls if we always generate a new XML file
	*--------------------------------------------------------------------------------------
	If This.lForce
		Return .T.
	EndIf

	*--------------------------------------------------------------------------------------
	* Read the current file attributes for the code file
	*--------------------------------------------------------------------------------------
	Local laFile[1]
	If ADir (laFile,m.tcFile) == 0
		Return .T.
	EndIf
	
	*--------------------------------------------------------------------------------------
	* Read the current file attributes for the generated text file. If this is the first
	* time and the generated file doesn't exist, than we report that the file has changed
	* to force a text generation process.
	*--------------------------------------------------------------------------------------
	Local laText[1]
	If ADir (laText,m.tcText) == 0
		Return .T.
	EndIf
	
	*--------------------------------------------------------------------------------------
	* Compare date and time.
	*--------------------------------------------------------------------------------------
	If laFile[3] > laText[3]
		Return .T.
	EndIf 
	If laFile[3] < laText[3]
		Return .F.
	EndIf 
	If laFile[4] > laText[4]
		Return .T.
	EndIf 

Return .F.


EndDefine



*========================================================================================
* Common base class for libraries (VCX, SCX)
*========================================================================================
Define Class CSplitLibraries as CSplit


*========================================================================================
* Sorts all procedures in a method
*========================================================================================
Function SortMethod
Lparameter tcMethod
	Local laCode[1], lcMethod, lcSorted, lnMethod, lnCount, lcSep
	lcSep =Chr(13)+Chr(10)
	lcSorted = ""
	lnCount = Occurs("ENDPROC"+lcSep,m.tcMethod)-1
	If lnCount=0
		lnCount = Occurs("ENDPROC",m.tcMethod)-1
		lcSep = []
	Endif
	Dimension laCode[m.lnCount]
	
	*--------------------------------------------------------------------------------------
	* parses the code line by line. We expect 
	*--------------------------------------------------------------------------------------
	Local laSource[1], lnLine
	For lnLine = 1 to ALines(laSource, m.tcMethod)
	EndFor 
	For lnMethod = 1 To m.lnCount
		laCode[m.lnMethod] = Strextract( ;
			M.tcMethod, ;
			"ENDPROC"+lcSep, ;
			"ENDPROC"+lcSep, ;
			M.lnMethod )
		laCode[m.lnMethod] = laCode[m.lnMethod] + "ENDPROC"+Chr(13)+Chr(10)
	Endfor
	Asort( laCode, -1, -1, 0, 1 )
	For Each lcMethod In laCode
		lcSorted = m.lcSorted + m.lcMethod
	Endfor
Return m.lcSorted


*========================================================================================
* OLE2 contains the physical name of the OCX or DLL when a record refers to an ActiveX
* control. On different developer machines these controls can be located in different
* folders without affecting the code. 
*
* When a control is stored outside the project directory, we assume that every developer
* is responsible for installing and registering the control. Therefore we only leave
* the file name which should be fixed. It's also sufficient for VFP to locate an OCX
* file when the control is not registered and the OCX file is stored in the current
* directory or the application path.
*========================================================================================
Procedure FixOle2Fields

	*--------------------------------------------------------------------------------------
	* Project directory for comparision purposes
	*--------------------------------------------------------------------------------------
	Local lcProjDir
	lcProjDir = Upper(Alltrim(This.cHomeDir))
	If Right(m.lcProjDir,1) == "\"
		lcProjDir = Left(m.lcProjDir, Len(m.lcProjDir)-1)
	EndIf
	
	*--------------------------------------------------------------------------------------
	* Check all OLE2 fields
	*--------------------------------------------------------------------------------------
	Local lcOcx
	Scan for not Empty(Ole2)
		lcOcx = StrExtract (Ole2, "OLEObject = ", Chr(13), 1, 2)
		If This.OcxOutsideProjDir (m.lcOcx, m.lcProjDir)
			This.TruncateOle2 (m.lcOcx)
		EndIf 
	EndScan 
	
EndProc


*========================================================================================
* Returns .T. when the OCX control resides outside the project directory
*========================================================================================
Function OcxOutsideProjDir (tcOcx, tcProjDir)

	Local lcOcxDir, llOutside
	lcOcxDir = Upper (JustPath (m.tcOcx))
	If Left(m.lcOcxDir, Len(m.tcProjDir)) == m.tcProjDir
		llOutside = .F.
	Else
		llOutside = .T.
	EndIf

Return m.llOutside


*========================================================================================
* �ndert eine OLE2 Feld auf ausschlie�lich den Dateinamen ab
*========================================================================================
Procedure TruncateOle2 (tcOcx)
	Replace Ole2 with Strtran ( ;
		 Ole2 ;
		,"OLEObject = " + m.tcOcx ;
		,"OLEObject = " + JustFname(m.tcOcx) ;
	)
EndProc


EndDefine 


*========================================================================================
* Handles VCX classes
*========================================================================================
Define Class CSPlitVCX as CSplitLibraries 

	cFile = ""

*========================================================================================
* Converts a class library into a set of XML file, one for the class library itself, and
* one for each class. The XML files are placed into the same directory as the class 
* library and only replaced if the contents has changed
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile) and This.HasChanged (This.cFile, This.cFile+".xml")
		USE (This.cFile) Alias VCX Shared In Select("VCX")
		This.GenerateHeader()
		Select ClassList
		Scan
			This.GenerateClass( Alltrim(ClassList.ObjName) )
		EndScan 
		USE in Select("VCX")
		USE in Select("ClassList")
		USE in Select("Class")
	EndIf 
EndProc


*========================================================================================
* Creates an XML file with just the classes in a library
*========================================================================================
Procedure GenerateHeader
	This.Msg( JustFname(This.cFile) )
	Select Lower(PadR(ObjName,128)) as ObjName ;
		from VCX ;
		Where Empty(Parent) ;
		  and Platform == "WINDOWS" ;
		  and Reserved1 == "Class" ;
		order by 1 ;
		into Cursor ClassList
	This.CursorToXML( "ClassList", This.cFile+".xml" )
EndProc


*========================================================================================
* Produces an XML file for a single class
*========================================================================================
Procedure GenerateClass
LParameter tcClass

	This.Msg( JustFname(This.cFile)+"."+m.tcClass )

	Local lnStart, lnEnd
	Select VCX
	Locate for objname == m.tcClass ;
		and Empty(Parent) ;
		and Platform == "WINDOWS " ; 
		and Reserved1 == "Class"
	lnStart = Recno()
	lnEnd = Recno() + Val(Reserved2)
	USE in Select("Class")
	
	Select *, Recno() as nRecNo ;
		From VCX ;
		Where Recno()	Between m.lnStart and m.lnEnd ;
		Nofilter ;
		ReadWrite ;
		into Cursor Class
	
	Alter Table Class Drop Column TimeStamp
	Alter Table Class Drop Column ObjCode
	Alter Table Class alter column OLE M NoCPTrans
	Go Bottom
	Blank FIELDS Reserved1    && time stamp of all include files
	Blank FIELDS properties   && Class designer settings: grid spacing, font size
	Scan for not Empty(Methods)
		Replace Methods with This.SortMethod("ENDPROC"+Chr(13)+Chr(10)+Methods)
	EndScan 
	Replace all ;
		 Properties with This.EncodeMemo(Properties) ;
		,Methods with This.EncodeMemo(Methods) ;
		,User with This.EncodeMemo(User) ;
		,Reserved4 with This.EncodeMemo(Reserved4) ;
		,Reserved5 with This.EncodeMemo(Reserved5) ;
		,Protected WITH This.EncodeMemo(Protected)

	This.FixOle2Fields()	
		
	Index on ;
		Iif(Platform=="WINDOWS ","1","2") + ;
		Padr(Padr(Parent,50)+padr(Sys(2007,Parent),10) + ;
		Padr(ObjName,50)+Padr(Sys(2007,ObjName),10),120) ;
	Tag _Fixed
	
	This.CursorToXML( "Class", This.cFile+"."+m.tcClass+".xml" )

EndProc




EndDefine 



*========================================================================================
* Handles SCX classes
*========================================================================================
Define Class CSPlitSCX as CSplitLibraries 

	cFile = ""

*========================================================================================
* Converts a class library into a set of XML file, one for the class library itself, and
* one for each class. The XML files are placed into the same directory as the class 
* library and only replaced if the contents has changed
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile) and This.HasChanged (This.cFile, This.cFile+".xml")
		This.GenerateForm()
	EndIf 
EndProc


*========================================================================================
* Produces an XML file for a single class
*========================================================================================
Procedure GenerateForm

	This.Msg( JustFname(This.cFile) )

	Select *, Recno() as nRecNo from (This.cFile) into Cursor Form Nofilter ReadWrite
	Blank All FIELDS ObjCOde		
	Blank all FIELDS TimeStamp
	Alter Table Form alter column OLE M NoCPTrans
	Go Bottom
	Blank FIELDS Reserved1    && time stamp of all include files
	Blank FIELDS properties   && Class designer settings: grid spacing, font size
	Scan for not Empty(Methods)
		Replace Methods with This.SortMethod("ENDPROC"+Chr(13)+Chr(10)+Methods)
	EndScan 
	Replace all Properties with This.EncodeMemo(Properties)
	Replace all Methods with This.EncodeMemo(Methods)
	Replace all User with This.EncodeMemo(User)
		
	This.FixOle2Fields()

	Index on ;
		Iif(Platform=="WINDOWS ","1","2") + ;
		Padr(Padr(Parent,50)+padr(Sys(2007,Parent),10) + ;
		Padr(ObjName,50)+Padr(Sys(2007,ObjName),10),120) ;
	Tag _Fixed
	
	This.CursorToXML( "Form", This.cFile+".xml" )
	
	USE in Select("Form")

EndProc
 

EndDefine 


*========================================================================================
* Handles PJX files
*========================================================================================
Define Class CSPlitPJX as CSplit

	cFile = ""

*========================================================================================
* Converts a class library into a set of XML file, one for the class library itself, and
* one for each class. The XML files are placed into the same directory as the class 
* library and only replaced if the contents has changed
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile)
		This.GenerateProject()
	EndIf 
EndProc


*========================================================================================
* Produces an XML file for a single class
*========================================================================================
Procedure GenerateProject

	This.Msg( JustFname(This.cFile) )

	Select * from (This.cFile) into Cursor Project Nofilter ReadWrite
	Alter Table Project Drop Column TimeStamp
	Alter Table Project Drop Column Symbols
	Blank FIELDS DevInfo for Recno()>1
	Blank FIELDS Object for Recno()>1
	Blank all fields CkVal
	blank all fields HomeDir
	Blank fields reserved1 for Type == "H"
	Blank fields object for Type == "H"
	Blank fields name for Type == "H"
	Alter Table Project alter column SCCData M NoCPTrans
	Alter Table Project alter column DevInfo M NoCPTrans
	

	Replace all Name with Chrtran(Name,Chr(0),"")
	Replace all Comments with Chrtran(Comments,Chr(0),"")
	Replace all Outfile with Chrtran(Outfile,Chr(0),"")
	Replace all Reserved1 with Chrtran(Reserved1,Chr(0),"")
	Replace Outfile with Chrtran(outfile,Chr(0),"") for Recno()==1
	Replace Object with Chrtran(Object,Chr(0),"") for Recno()==1

	This.CursorToXML( "Project", ForceExt(This.cFile,"twofox") )
	
	USE in Select("Project")

EndProc

	
EndDefine 


*========================================================================================
* Handles FRX classes
*========================================================================================
Define Class CSPlitFRX as CSplit

	cFile = ""

*========================================================================================
* Converts a class library into a set of XML file, one for the class library itself, and
* one for each class. The XML files are placed into the same directory as the class 
* library and only replaced if the contents has changed
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile) and This.HasChanged (This.cFile, This.cFile+".xml")
		This.GenerateReport()
	EndIf 
EndProc


*========================================================================================
* Produces an XML file for a single report
*========================================================================================
Procedure GenerateReport

	This.Msg( JustFname(This.cFile) )

	Select * from (This.cFile) into Cursor Report Nofilter ReadWrite
	Blank FIELDS Tag, Tag2 for Recno() == 1
	Blank FIELDS Tag2 for InList(OBJTYPE,25,26)
	Replace all Expr with Chrtran(Expr,Chr(0),"")
	
	This.CursorToXML( "Report", This.cFile+".xml" )
	USE in Select("Report")

EndProc
 
	
EndDefine 


*========================================================================================
* Handles menu files (MNX)
*========================================================================================
Define Class CSPlitMNX as CSplit

	cFile = ""

*========================================================================================
* Converts an MNX file into a single XML file
*========================================================================================
Procedure Convert
LParameter tcFile
	This.cFile = m.tcFile
	If File(This.cFile) and This.HasChanged (This.cFile, This.cFile+".xml")
		This.GenerateMenu()
	EndIf 
EndProc


*========================================================================================
* Produces an XML file for the menu
*========================================================================================
Procedure GenerateMenu

	This.Msg( JustFname(This.cFile) )

	Select * from (This.cFile) into Cursor Menu Nofilter ReadWrite

	Alter Table Menu alter column Mark C(5)
	
	Replace all Mark with This.EncodeMemo(Mark)
	Replace all Procedure with This.EncodeMemo(Procedure)
	Replace all Prompt with This.EncodeMemo(Prompt)
	
	This.CursorToXML( "Menu", This.cFile+".xml" )
	USE in Select("Menu")

EndProc

	
EndDefine 


*========================================================================================
* Performs a conversion of a FoxPro file. 
*========================================================================================
Define Class CConverter as Custom

	cHomeDir = ""
	lForce = .F.
	oFactory = NULL

*========================================================================================
* A factory is injected here that defines what kind of conversion is performed
*========================================================================================
Procedure Init( toFactory )

	*--------------------------------------------------------------------------------------
	* Assertions
	*--------------------------------------------------------------------------------------
	Assert Vartype(m.toFactory) == "O"
	
	*--------------------------------------------------------------------------------------
	* Save dependencies
	*--------------------------------------------------------------------------------------
	This.oFactory = m.toFactory

EndProc


*========================================================================================
* Before we can release the converter, we have to remove the factory from memory.
*========================================================================================
Procedure Release
	This.oFactory = NULL
EndProc


*========================================================================================
* If no file type is specified, the file extension determines the type.
*========================================================================================
Procedure Convert( tcFileName, tcType )

	*--------------------------------------------------------------------------------------
	* Assertions
	*--------------------------------------------------------------------------------------
	Assert Vartype(m.tcFileName) == "C"
	Assert Vartype(m.tcType) $ "CL"
	
	*--------------------------------------------------------------------------------------
	* Determine the file type
	*--------------------------------------------------------------------------------------
	Local lcType
	If Vartype(m.tcType) == "C"
		lcType = Lower(Alltrim(m.tcType))
	Else 
		lcType = This.InferType(m.tcFileName)
	EndIf
	
	*--------------------------------------------------------------------------------------
	* Convert the file based on the type
	*--------------------------------------------------------------------------------------
	Local loConverter, lcFileName
	loConverter = This.oFactory.GetObject(m.lcType)
	loConverter.cHomeDir = This.cHomeDir
	lcFileName = This.GetName(m.tcFileName)
	loConverter.Convert( m.lcFileName )
	
EndProc

*========================================================================================
* Infers the type of a file from its file extension
*========================================================================================
Procedure InferType( tcFileName)

	Local lcExtension, lcType
	lcExtension = JustExt(Lower(m.tcFileName))
	
	Do case
	Case m.lcExtension == "scx"
		lcType = "scx"
	Case m.lcExtension == "vcx"
		lcType = "vcx"
	Case m.lcExtension == "frx"
		lcType = "frx"
	Case m.lcExtension == "lbx"
		lcType = "frx"
	Case m.lcExtension == "mnx"
		lcType = "mnx"
	Case m.lcExtension == "pjx"
		lcType = "pjx"
	Otherwise
		lcType = ""
	EndCase
	
Return m.lcType

*========================================================================================
* Returns the full path to the file. Adds the home directory if required.
*========================================================================================
Procedure GetName( tcFileName )

	Local lcFileName
	lcFileName = m.tcFileName
	If not ":" $ m.tcFileName and not "\\" $ m.tcFileName
		lcFileName = Addbs(This.cHomeDir) + m.tcFileName
	EndIf

Return m.lcFileName

EndDefine 


*========================================================================================
* This factory class returns a CSplit object based on the type
*========================================================================================
Define Class CSplitFactory as Custom

Procedure GetObject( tcType )
	Local loSplit
	DO case
	Case m.tcType == "vcx"
		loSplit = NewObject("CSPlitVCX","TwoFox.prg")
	Case m.tcType == "scx"
		loSplit = NewObject("CSPlitSCX","TwoFox.prg")
	Case m.tcType == "frx"
		loSplit = NewObject("CSPlitFRX","TwoFox.prg")
	Case m.tcType == "mnx"
		loSplit = NewObject("CSPlitMNX","TwoFox.prg")
	Otherwise 
		loSplit = NULL
	EndCase 
Return m.loSplit

EndDefine 


*========================================================================================
* This factory class returns a CMerge object based on the type
*========================================================================================
Define Class CMergeFactory as Custom

Procedure GetObject( tcType )
	Local loSplit
	DO case
	Case m.tcType == "vcx"
		loSplit = NewObject("CMergeVCX","TwoFox.prg")
	Case m.tcType == "scx"
		loSplit = NewObject("CMergeSCX","TwoFox.prg")
	Case m.tcType == "frx"
		loSplit = NewObject("CMergeFRX","TwoFox.prg")
	Case m.tcType == "mnx"
		loSplit = NewObject("CMergeMNX","TwoFox.prg")
	Otherwise 
		loSplit = NULL
	EndCase 
Return m.loSplit

EndDefine 
