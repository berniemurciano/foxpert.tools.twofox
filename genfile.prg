*========================================================================================
* Converts a single file back to its binary format. You can use this when you know which
* file you want to convert. 
*
* For libraries make sure you pick the *.vcx.xml file, not any of the individual class
* files.
*========================================================================================
LParameter tcFile

	*--------------------------------------------------------------------------------------
	* Requires VFP 8 or higher, because VFP 7 has some problems with its XML support
	*--------------------------------------------------------------------------------------
	If not Version(4) >= "08.00"
		MessageBox("GenFile requires VFP 8.0 or higher")
		Return
	EndIf
	
	*--------------------------------------------------------------------------------------
	* A few settings
	*--------------------------------------------------------------------------------------
	Set Deleted on
	
	*--------------------------------------------------------------------------------------
	* Prompt for a file if none has been passed in
	*--------------------------------------------------------------------------------------
	Local lcFile
	If Vartype(m.tcFile) == "C" and File(m.tcFile)
		lcFile = m.tcFile
	Else
		lcFile = GetFile( "xml", "Select an xml file" )
		If not File(m.lcFile)
			Return
		EndIf 
	EndIf
	
	*--------------------------------------------------------------------------------------
	* Convert the file back to the binary form.
	*--------------------------------------------------------------------------------------
	Local lcExtension, loConverter, loFactory, lcExtension
	loFactory = NewObject("CMergeFactory","TwoFox.prg")
	loConverter = NewObject("CConverter","TwoFox.prg","",m.loFactory)
	lcFile = Addbs(JustPath(m.lcFile)) + JustStem(m.lcFile)
	lcExtension = Lower(JustExt(m.lcFile))
	loConverter.Convert(m.lcFile, m.lcExtension)
